dd if=/dev/zero of=/swapfile bs=1G count=4 status=progress

chmod 600 /swapfile

mkswap /swapfile

swapon /swapfile

echo  "/swapfile	none	swap	defaults	0 0" >> /etc/fstab

ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime

hwclock --systohc

echo en_US.UTF-8 UTF-8 >> /etc/locale.gen

echo en_US ISO-8859-1 >> /etc/locale.gen

locale-gen

echo LANG=en_US.UTF-8 >> /etc/locale.conf 

echo hades >> /etc/hostname

passwd 

pacman -S grub dhcpcd connman-openrc --needed --noconfirm

grub-install /dev/sda

grub-mkconfig -o /boot/grub/grub.cfg 

rc-update add connmand

useradd -mG wheel abraxas

passwd abraxas

EDITOR=vim visudo

pacman -Sy archlinux-keyring artix-keyring --noconfirm

rm -r /etc/pacman.d/gnupg

pacman-key --init

pacman-key --populate archlinux artix

pacman -Scc --noconfirm

pacman -Syyu --noconfirm

exit

